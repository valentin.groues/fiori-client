#!/usr/bin/env python
# coding=utf-8
import calendar
import datetime
import json
import logging
import os
from io import StringIO

import requests
from lxml import etree
from requests_ntlm import HttpNtlmAuth

PAYSTUB_PDF_URL = "https://fiori.uni.lu/sap/opu/odata/sap/HCMFAB_MYPAYSTUBS_SRV/PaystubDownloadSet(EmployeeNumber='{}',SequenceNumber={})/$value"
STS_BASE_URL = "https://sts.uni.lu"
FIORI_BASE_URL = "https://fiori.uni.lu"
FIORI_URL = FIORI_BASE_URL + "/fiori/"
CALENDAR_WORK_DAYS_URL = FIORI_BASE_URL + "/sap/opu/odata/sap/Z_HCMFAB_TIMESHEET_MAINT_170_SRV/WorkCalendarCollection?sap-client=400&$expand=TimeEntries&$filter=StartDate%20eq%20datetime%27{}T00%3a00%3a00%27%20and%20EndDate%20eq%20datetime%27{}T00%3a00%3a00%27%20and%20Pernr%20eq%20%27{}%27%20and%20ProfileId%20eq%20%27Z_FTS%27"
CALENDAR_RECORDED_TIMES = FIORI_BASE_URL + "/sap/opu/odata/sap/HCM_TIMESHEET_MAN_SRV/TimeDataList?$filter=Pernr%20eq%20%27{}%27%20and%20StartDate%20eq%20%27{}%27%20and%20EndDate%20eq%20%27{}%27"
CALENDAR_BATCH_URL = FIORI_BASE_URL + "/sap/opu/odata/sap/HCM_TIMESHEET_MAN_SRV/$batch"
CALENDAR_PDF_URL = FIORI_BASE_URL + "/sap/opu/odata/sap/HCM_TIMESHEET_MAN_SRV/PDFTimeSheetSet(KeyMonth='{0}{1:02d}')/$value"
TIMESHEETS_BATCH_URL = FIORI_BASE_URL + "/sap/opu/odata/sap/Z_HCMFAB_TIMESHEET_MAINT_170_SRV/$batch?sap-client=400"
GET_PERNR_CODE_URL = "https://fiori.uni.lu/sap/opu/odata/sap/HCMFAB_COMMON_SRV/ConcurrentEmploymentSet?$filter=ApplicationId%20eq%20%27MYPAYSTUBS%27"
CSRF_FETCH_URL = 'https://fiori.uni.lu/sap/opu/odata/sap/HCMFAB_COMMON_SRV/'
__author__ = 'Valentin Grouès'

ACCEPT_HTML = "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7"
ACCEPT_ENCODING_GZIP = "gzip, deflate, br, zstd"
ACCEPT_LANGUAGE_ENGLISH_US = "en-US,en;q=0.9"
USER_AGENT_MOZILLA_MACOS = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Mobile Safari/537.36"

headers_sts = {
    "Accept": ACCEPT_HTML,
    "Accept-Encoding": ACCEPT_ENCODING_GZIP,
    "Accept-Language": ACCEPT_LANGUAGE_ENGLISH_US,
    "Connection": "keep-alive",
    "DNT": "1",
    "Host": "sts.uni.lu",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": USER_AGENT_MOZILLA_MACOS
}

headers_fiori = {
    "Accept": ACCEPT_HTML,
    "Accept-Encoding": ACCEPT_ENCODING_GZIP,
    "Accept-Language": ACCEPT_LANGUAGE_ENGLISH_US,
    "Host": "fiori.uni.lu",
    "User-Agent": USER_AGENT_MOZILLA_MACOS
}
headers_new_fiori = {
    "Accept": ACCEPT_HTML,
    "Accept-Encoding": ACCEPT_ENCODING_GZIP,
    "Accept-Language": ACCEPT_LANGUAGE_ENGLISH_US,
    "Host": "sts.uni.lu",
    "User-Agent": USER_AGENT_MOZILLA_MACOS,
    "Content-Type": "application/x-www-form-urlencoded",
    "Pragma": "no-cache",
    "Cache-Control": "max-age=0",
    "Connection": "keep-alive",
    "Origin": "https://fiori.uni.lu",
    "Referer": "https://fiori.uni.lu/"
    # "Content-Length": "1877"
}
headers_new_fiori_3 = headers_new_fiori.copy()
headers_new_fiori_3.update({
    "Host": "fiori.uni.lu",
    "Origin": "https://sts.uni.lu",
    "Referer": "https://sts.uni.lu/",
    "Upgrade-Insecure-Requests": "1"
})

headers_fiori_post = {
    "Accept": ACCEPT_HTML,
    "Accept-Encoding": ACCEPT_ENCODING_GZIP,
    "Accept-Language": ACCEPT_LANGUAGE_ENGLISH_US,
    "Host": "fiori.uni.lu",
    "User-Agent": USER_AGENT_MOZILLA_MACOS,
    "Connection": "keep-alive",
    "Upgrade-Insecure-Requests": "1",
    "Content-Type": "application/x-www-form-urlencoded",
    "Content-Length": "8006"
}

headers_timesheets_ajax = {
    "Host": "fiori.uni.lu",
    "User-Agent": USER_AGENT_MOZILLA_MACOS,
    "Accept": "application/json",
    "Accept-Language": ACCEPT_LANGUAGE_ENGLISH_US,
    "Accept-Encoding": ACCEPT_ENCODING_GZIP,
    "X-Requested-With": "XMLHttpRequest",
    "X-XHR-Logon": 'accept="iframe"',
    "Referer": "https://fiori.uni.lu/fiori",
    "Connection": "keep-alive",
}
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.INFO)
parser = etree.HTMLParser()


class Record(object):
    def __init__(self, day, record_number):
        self.day = day
        self.record_number = record_number
        self.project_code = None
        self.task = None
        self.task_type = None
        self.hours = None
        self.notes = None
        self.start_time = None
        self.end_time = None
        self.counter = None
        self.reason = None
        self.status = None

    def is_draft(self):
        return self.status == 'MSAVE'


def parse_form(request):
    content = request.text
    root = etree.parse(StringIO(content), parser)
    form = root.find('body').find('form')
    target_url = form.get('action')
    inputs = form.findall('input')
    field_name_1 = inputs[0].get('name')
    field_value_1 = inputs[0].get('value')
    field_name_2 = inputs[1].get('name')
    field_value_2 = inputs[1].get('value')
    return target_url, {field_name_1: field_value_1, field_name_2: field_value_2}


class FioriClient:
    def __init__(self, domain):
        self._employee_code = None
        self.cookies = None
        self.domain = domain
        self.session = requests.Session()

    def login(self, username, password):
        logger.info("Login user '%s'", username)
        logger.debug("Request 1: %s", FIORI_URL)

        s = self.session
        auth = HttpNtlmAuth("{}\\{}".format(self.domain, username), password)
        # auth = HTTPBasicAuth(username, password)
        request = s.get(FIORI_URL, allow_redirects=False, headers=headers_fiori)
        assert request.ok
        # this results in a 302, redirect to sts.uni.lu
        target_url, data = parse_form(request)
        # 'https://sts.uni.lu/adfs/ls/'
        r = s.post(target_url, data=data,
                   allow_redirects=False, headers=headers_new_fiori)
        assert r.ok
        url2 = STS_BASE_URL + r.next.path_url
        logger.debug("Request 2: %s", url2)
        # 'https://sts.uni.lu/adfs/ls/wia?client-request-id=cd1b29b3-08cf-4300-af28-0080010000ba'
        r2 = s.post(url2, data=data,
                    allow_redirects=False, headers=headers_sts, auth=auth)
        assert r2.ok
        target_url2, data2 = parse_form(r2)
        r3 = s.post(target_url2, data=data2, allow_redirects=False, headers=headers_new_fiori_3, auth=auth)
        assert r3.ok
        _, data3 = parse_form(r3)
        r4 = s.post(FIORI_URL, data=data3, allow_redirects=False, headers=headers_new_fiori_3)
        assert r4.ok
        r5 = s.get(FIORI_URL, allow_redirects=False, headers=headers_new_fiori_3)
        assert r5.ok
        logger.info("Success")

    @property
    def employee_code(self):
        if not self._employee_code:
            headers = {
                "Host": "fiori.uni.lu",
                "User-Agent": USER_AGENT_MOZILLA_MACOS,
                "Accept": "application/json",
                "Accept-Language": "en",
                "Accept-Encoding": ACCEPT_ENCODING_GZIP,
                'MaxDataServiceVersion': '2.0',
                'DataServiceVersion': '2.0',
                'X-XHR-Logon': 'accept="iframe"',
                'X-Requested-With': 'XMLHttpRequest',
                'Referer': 'https://fiori.uni.lu/fiori',
                'Connection': 'keep-alive',
            }
            request = self.session.get(GET_PERNR_CODE_URL, headers=headers, cookies=self.cookies)
            assert request.ok
            json_response = request.json()
            self._employee_code = json_response["d"]["results"][0]['EmployeeId']

        return self._employee_code

    def get_paystub_pdf(self, entry, directory=""):
        period = entry["PayrollPeriod"]
        logger.info('Downloading paystub for period {}'.format(period))
        sequence = entry["SequenceNumber"]
        personnel = entry["EmployeeNumber"]
        url = PAYSTUB_PDF_URL.format(personnel, sequence)
        request = self.session.get(url, stream=True)
        assert request.ok
        file_path = os.path.join(directory, "{}.pdf".format(period))
        with open(file_path, 'wb') as f:
            for chunk in request:
                f.write(chunk)
        logger.info("Done")

    def get_pdf(self, year, month, file_path):
        logger.info('Downloading pdf for month {} of {}'.format(month, year))
        request = requests.get(CALENDAR_PDF_URL.format(year, month), cookies=self.cookies, stream=True)
        assert request.ok
        with open(file_path, 'wb') as f:
            for chunk in request:
                f.write(chunk)
        logger.info("Done")

    def get_csrf_token(self):
        headers = {
            "Host": "fiori.uni.lu",
            "User-Agent": USER_AGENT_MOZILLA_MACOS,
            "Accept": "application/json",
            "Accept-Language": "en",
            "Accept-Encoding": ACCEPT_ENCODING_GZIP,
            'MaxDataServiceVersion': '2.0',
            'DataServiceVersion': '2.0',
            'X-CSRF-Token': 'Fetch',
            'X-XHR-Logon': 'accept="iframe"',
            'X-Requested-With': 'XMLHttpRequest',
            'Referer': 'https://fiori.uni.lu/fiori',
            'Connection': 'keep-alive',
        }
        request = self.session.get(CSRF_FETCH_URL, headers=headers, cookies=self.cookies)
        csrf_token = request.headers['X-CSRF-Token']
        return csrf_token

    def get_approval_data(self):
        csrf_token = self.get_csrf_token()
        url = "https://fiori.uni.lu/sap/opu/odata/sap/HCM_TIMESHEET_APPROVE_SRV/TIME_PENDING"
        headers = {
            "Accept-Language": "en",
            "Accept": "application/json",
            'MaxDataServiceVersion': '2.0',
            'DataServiceVersion': '2.0',
            'x-csrf-token': csrf_token,
            "Host": "fiori.uni.lu",
            "User-Agent": USER_AGENT_MOZILLA_MACOS
        }
        request = requests.get(url, headers=headers, cookies=self.cookies)
        assert request.ok
        json_response = request.json()
        entries = json_response["d"]["results"]
        return entries

    def get_paystubs(self):
        csrf_token = self.get_csrf_token()

        url = "https://fiori.uni.lu/sap/opu/odata/sap/HCMFAB_MYPAYSTUBS_SRV/EmployeeAssignmentSet('{}')/toPaystubs?sap-client=400&$expand=toPaystubDisplay%2ctoPaystubDownload"
        url = url.format(self.employee_code)
        headers = {
            "Accept-Language": "en",
            "Accept": "application/json",
            'MaxDataServiceVersion': '2.0',
            'DataServiceVersion': '2.0',
            'x-csrf-token': csrf_token,
            "Host": "fiori.uni.lu",
            "User-Agent": USER_AGENT_MOZILLA_MACOS
        }
        request = self.session.get(url, headers=headers)
        assert request.ok
        json_response = request.json()
        entries = json_response["d"]["results"]
        return entries

    def get_records_details_to_approve(self, entry):
        csrf_token = self.get_csrf_token()
        url = "https://fiori.uni.lu/sap/opu/odata/sap/HCM_TIMESHEET_APPROVE_SRV/TIME_DETAILS_EMP?$filter={}"
        headers = {
            "Accept-Language": "en",
            "Accept": "application/json",
            'MaxDataServiceVersion': '2.0',
            'DataServiceVersion': '2.0',
            'x-csrf-token': csrf_token,
            "Host": "fiori.uni.lu",
            "User-Agent": USER_AGENT_MOZILLA_MACOS
        }
        today = datetime.datetime.now().strftime('%Y%m%d')
        filter = "PERNR%20eq%20'{}'%20and%20STARTDATE%20eq%20'{}'and%20ENDDATE%20eq%20'{}'".format(entry['PERNR'],
                                                                                                   entry['STARTDATE'],
                                                                                                   today)
        url = url.format(filter)
        request = requests.get(url, headers=headers, cookies=self.cookies)
        assert request.ok
        json_response = request.json()
        entries = json_response["d"]["results"]
        return entries

    def approve_entry(self, employee_id, entry):
        csrf_token = self.get_csrf_token()
        url = "https://fiori.uni.lu/sap/opu/odata/sap/HCM_TIMESHEET_APPROVE_SRV/CATS_ACTION?cats={}"
        headers = {
            "Accept-Language": "en",
            "Accept": "application/json",
            'MaxDataServiceVersion': '2.0',
            'DataServiceVersion': '2.0',
            'x-csrf-token': csrf_token,
            "Content-Type": "application/http",
            "Content-Transfer-Encoding": "binary"
            # "Host": "fiori.uni.lu",
            # "User-Agent": USER_AGENT_MOZILLA_MACOS
        }
        changed_time = entry['CHANGED_TIME'][2:4] + entry['CHANGED_TIME'][5:7] + entry['CHANGED_TIME'][8:10]
        changed_date_timestamp = int(entry['CHANGED_DATE'][6:-5])
        changed_date = datetime.datetime.fromtimestamp(changed_date_timestamp)
        changed_date = changed_date.strftime('%Y%m%d')
        cats_entry = "{},{},A,,{},{}".format(employee_id, entry['COUNTER'], changed_date,
                                             changed_time)
        # cats_edntries = urllib.parse.quote_plus(cats_entry+"/")
        cats_entries = "'" + cats_entry + "/'"
        url = url.format(cats_entries)  # delimited by / if many
        request = requests.post(url, headers=headers, cookies=self.cookies)
        return request.ok

    def submit_record(self, record):
        if not record.is_draft():
            logger.warn("record not in draft status, aborting!")
            return
        day_string = record.day.strftime('%Y-%m-%dT00:00:00')
        csrf_token = self.get_csrf_token()
        url = "https://fiori.uni.lu/sap/opu/odata/sap/HCM_TIMESHEET_MAN_SRV/TimeEntries"
        headers = {
            "Accept-Language": "en",
            "Accept": "application/json",
            'MaxDataServiceVersion': '2.0',
            'DataServiceVersion': '2.0',
            'x-csrf-token': csrf_token,
            "Host": "fiori.uni.lu",
            "User-Agent": USER_AGENT_MOZILLA_MACOS
        }
        awart = "{0:04.0f}".format(float(record.hours) * 100)
        json_message = {"Counter": record.counter, "TimeEntryOperation": "U",
                        "TimeEntryDataFields": {"WORKDATE": day_string, "CATSAMOUNT": record.hours,
                                                "BEGUZ": record.start_time,
                                                "ENDUZ": record.end_time, "POSID": record.project_code,
                                                "TASKLEVEL": record.task,
                                                "TASKTYPE": record.task_type,
                                                "AWART": awart}, "TimeEntryRelease": "X"}
        request = requests.post(url, json=json_message, headers=headers, cookies=self.cookies)
        return request.ok

    def post_time_entry_direct(self, day, hours, assignment):
        assignment_name = assignment.get('AssignmentName', "")
        project_code = assignment.get('AssignmentFields', {}).get('POSID', "")
        rproj = assignment.get('AssignmentFields', {}).get('RPROJ', "")
        task = assignment.get('AssignmentFields', {}).get('TASKLEVEL', "")
        task_type = assignment.get('AssignmentFields', {}).get('TASKTYPE', "")
        day_string = day.strftime('%Y-%m-%d')
        csrf_token = self.get_csrf_token()
        json_message = {
            "AllowEdit": "",
            "AllowRelease": "X",
            "AssignmentId": "20240430094637.9870950",
            "AssignmentName": assignment_name,
            "CatsDocNo": "",
            "Counter": "",
            "Pernr": self.employee_code,
            "RefCounter": "",
            "RejReason": "",
            "Status": "",
            "TimeEntryOperation": "C",
            "TimeEntryDataFields": {
                "__metadata": {
                    "type": "ZHCMFAB_TIMESHEET_MAINT_SRV.TimeEntryDataFields"
                },
                "AWART": "",
                "LSTAR": "",
                "ALLDF": "",
                "PRAKN": "",
                "PRAKZ": "",
                "BEMOT": "",
                "CATSHOURS": hours,
                "PERNR": "",
                "KOKRS": "",
                "CPR_EXTID": "",
                "CPR_GUID": "",
                "CPR_OBJGEXTID": "",
                "CPR_OBJGUID": "",
                "CPR_OBJTYPE": "",
                "WAERS": "",
                "ENDUZ": "000000",
                "EXTAPPLICATION": "",
                "EXTDOCUMENTNO": "",
                "AUFKZ": "",
                "EXTSYSTEM": "",
                "AUERU": "",
                "FUNC_AREA": "",
                "FUND": "",
                "GRANT_NBR": "",
                "LONGTEXT": "",
                "RNPLNR": "",
                "VERSL": "",
                "ERUZU": "",
                "TRFGR": "",
                "TRFST": "",
                "WERKS": "",
                "SEBELP": "",
                "SEBELN": "",
                "PLANS": "00000000",
                "VTKEN": "",
                "CATSQUANTITY": "0.000",
                "RKOSTL": "",
                "RAUFNR": "",
                "RPRZNR": "",
                "RKSTR": "",
                "RKDPOS": "",
                "RKDAUF": "",
                "OFMNW": "0.0",
                "SPRZNR": "",
                "SKOSTL": "",
                "S_FUNC_AREA": "",
                "S_FUND": "",
                "S_GRANT_NBR": "",
                "LSTNR": "",
                "LTXA1": "",
                "SPLIT": "000",
                "BEGUZ": "000000",
                "STATKEYFIG": "",
                "TCURR": "",
                "REINR": "",
                "MEINH": "",
                "UNIT": "",
                "LGART": "",
                "WORKDATE": "{}T00:00:00".format(day_string),
                "WTART": "",
                "CATSAMOUNT": "0.00",
                "PRICE": "0.00",
                "RPROJ": rproj,
                "ARBPL": "",
                "TASKTYPE": task_type,
                "TASKLEVEL": task,
                "TASKCOMPONENT": "",
                "VORNR": "",
                "UVORN": "",
                "KAPAR": "",
                "BWGRL": "0.00",
                "LONGTEXT_DATA": "",
                "WORKITEMID": "",
                "POSID": project_code,
                "RAUFPL": "0000000000",
                "RAPLZL": "00000000",
                "PAOBJNR": "0000000000",
                "BUDGET_PD": "",
                "SBUDGET_PD": "",
                "KAPID": "00000000",
                "WABLNR": "",
                "OTYPE": "",
                "ARBID": "00000000",
                "AUTYP": "00",
                "HRCOSTASG": "",
                "HRKOSTL": "",
                "HRLSTAR": "",
                "HRFUND": "",
                "HRFUNC_AREA": "",
                "HRGRANT_NBR": "",
                "BUKRS": "",
                "HRBUDGET_PD": "",
                "ERNAM": "",
                "AENAM": "",
                "APNAM": "",
                "LOGSYS": "",
                "STATUS": "",
                "REFCOUNTER": "",
                "REASON": "",
                "BELNR": "",
                "TASKCOUNTER": "",
                "BEDID": "000000000000",
                "ZAMPM": "",
                "ZWPID": "",
                "ZWPACTVT": "",
                "ZCATSTEXT": "",
                "ZZRSBPN": "00000000",
                "ZZRSBNA": "",
                "ZZAP1ST": "",
                "ZZAP2ST": "",
                "ZZINHRS": False,
                "ZZCOUNT": "",
                "ZZEBELN": "",
                "ZZEBELP": "00000",
                "ZCONTKIND": True
            },
            "CheckOnly": "",
            "RecRowNo": "1",
            "__metadata": {
                "type": "ZHCMFAB_TIMESHEET_MAINT_SRV.TimeEntry"
            }
        }
        headers_batch = {
            "Accept": "multipart/mixed",
            "Accept-Language": "en",
            "Connection": "keep-alive",
            "Content-Type": "multipart/mixed; boundary=batch_123456",
            "DataServiceVersion": "2.0",
            "Host": "fiori.uni.lu",
            "MaxDataServiceVersion": "2.0",
            "Origin": "https://fiori.uni.lu",
            "Referer": "https://fiori.uni.lu/fiori",
            "sap-cancel-on-close": "false",
            "sap-contextid-accept": "header",
            "User-Agent": USER_AGENT_MOZILLA_MACOS,
            "x-csrf-token": csrf_token,
            "X-Requested-With": "XMLHttpRequest",
            "X-XHR-Logon": 'accept="iframe,strict-window,window"'
        }
        data = f"""
--batch_123456
Content-Type: multipart/mixed; boundary=changeset_123456

--changeset_123456
Content-Type: application/http
Content-Transfer-Encoding: binary

POST TimeEntryCollection?sap-client=400 HTTP/1.1
Content-Type: application/json
sap-contextid-accept: header
Accept: application/json
x-csrf-token: {csrf_token}
Accept-Language: en
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0

{json.dumps(json_message)}
--changeset_123456--

--batch_123456--
"""
        request = self.session.post(TIMESHEETS_BATCH_URL, data=data, headers=headers_batch)
        return request.ok

    def get_assignments(self, start_date, end_date):
        assignments_url = FIORI_BASE_URL + "/sap/opu/odata/sap/Z_HCMFAB_TIMESHEET_MAINT_170_SRV/AssignmentCollection?sap-client=400&$expand=ToGrps&$filter=ValidityStartDate%20eq%20datetime%27{}T22%3a00%3a00%27%20and%20ValidityEndDate%20eq%20datetime%27{}T22%3a00%3a00%27%20and%20Pernr%20eq%20%27{}%27"
        assignments_url = assignments_url.format(start_date, end_date, self.employee_code)
        csrf_token = self.get_csrf_token()
        headers = {
            "Accept-Language": "en",
            "Accept": "application/json",
            'MaxDataServiceVersion': '2.0',
            'DataServiceVersion': '2.0',
            'x-csrf-token': csrf_token,
            "Host": "fiori.uni.lu",
            "User-Agent": USER_AGENT_MOZILLA_MACOS
        }
        request_assignments = self.session.get(assignments_url, headers=headers,
                                            allow_redirects=False)
        assert request_assignments.ok
        json = request_assignments.json()
        # filter out internal hours assignments
        results = json['d']['results']
        return results
    def get_working_days(self, start_date, end_date):
        calendar_url = CALENDAR_WORK_DAYS_URL.format(start_date, end_date, self.employee_code)
        csrf_token = self.get_csrf_token()
        headers = {
            "Accept-Language": "en",
            "Accept": "application/json",
            'MaxDataServiceVersion': '2.0',
            'DataServiceVersion': '2.0',
            'x-csrf-token': csrf_token,
            "Host": "fiori.uni.lu",
            "User-Agent": USER_AGENT_MOZILLA_MACOS
        }
        request_calendar = self.session.get(calendar_url, headers=headers,
                                        allow_redirects=False)
        assert request_calendar.ok
        json = request_calendar.json()
        return json['d']['results']

    def get_recorded_entries(self, start_date, end_date):
        calendar_url = CALENDAR_RECORDED_TIMES.format(self.employee_code, start_date, end_date)
        request_calendar = requests.get(calendar_url, headers=headers_timesheets_ajax,
                                        allow_redirects=False,
                                        cookies=self.cookies)
        assert request_calendar.ok
        json_response = request_calendar.json()
        entries = self.build_records(json_response["d"]["results"])
        return entries

    def fill_timesheet(self, year, month, project_code, task, goal_hours=72.0):
        logger.info('Filling timesheet for month {} of {}'.format(month, year))
        _, last_day = calendar.monthrange(year, month)
        work_days = self.get_working_days("{0}-{1:02d}-01".format(year, month),
                                          "{0}-{1:02d}-{2}".format(year, month, last_day))
        assignments = self.get_assignments("{0}-{1:02d}-01".format(year, month),
                                           "{0}-{1:02d}-{2}".format(year, month, last_day))
        # get assignment matching project code
        for assignment in assignments:
            if assignment['AssignmentFields']['POSID'] == project_code and assignment['AssignmentFields']['TASKLEVEL'] == task:
                project_assignment = assignment
            if assignment['AssignmentFields']['TASKTYPE'] == 'INTE':
                internal_assignment = assignment
        total = 0
        for day in work_days:
            day_date_string = day['CaleDate']
            if day['IsWorkingDay'] == 'TRUE':
                logger.info("%s is a working day, creating record", day_date_string)
                time_entries = day.get('TimeEntries', {}).get('results', [{}])
                hours_already_filled = sum(float(entry.get('TimeEntryDataFields', {}).get('CATSHOURS', 0)) for entry in time_entries)
                target_hours = day['TargetHours'].strip()
                original_target_hours_float = float(target_hours) - hours_already_filled
                target_hours_float = original_target_hours_float
                if not target_hours_float:
                    logger.info("No hours to fill for this day")
                    continue
                day_date = datetime.datetime.strptime(day_date_string, '%Y%m%d').date()
                missing_hours = 0
                if total + original_target_hours_float > goal_hours:
                    missing_hours = target_hours_float - (goal_hours - total)
                    target_hours_float = goal_hours - total
                    logger.info("Creating %s hours record for internal hours", missing_hours)
                    result = self.post_time_entry_direct(day_date, str(missing_hours), internal_assignment)
                    if result:
                        logger.info("created successfully")
                    else:
                        logger.error("an error occurred")
                if missing_hours < original_target_hours_float:
                    logger.info("Creating %s hours record for project hours", target_hours_float)
                    result = self.post_time_entry_direct(day_date,  str(target_hours_float), project_assignment)
                    if result:
                        total += target_hours_float
                        logger.info("created successfully")
                    else:
                        logger.error("an error occurred")
            else:
                logger.info("%s is a non working day, skipping", day_date_string)
        logger.info('Timesheet for month {} of {} completed!'.format(month, year))

    def submit_timesheet(self, year, month):
        logger.info('Submitting timesheet for month {} of {}'.format(month, year))
        records = self.get_recorded_entries("{0}{1:02d}01".format(year, month),
                                            "{0}{1:02d}31".format(year, month))
        for record in records:
            if record.is_draft():
                logger.info("Submitting record for day %s", record.day.strftime('%Y%m%d'))
                status = self.submit_record(record)
                if status:
                    logger.info("record submitted")
                else:
                    logger.error("an error occurred while submitting this record")
        logger.info('Timesheet for month {} of {} submitted!'.format(month, year))

    @staticmethod
    def build_records(json_entries):
        records = []
        record = None
        for entry in json_entries:
            if entry['FieldName'] == 'WORKDATE':
                # first field, we create new record
                date_string = entry['FieldValue']
                record_number = entry['RecordNumber']
                day = datetime.datetime.strptime(date_string, '%Y%m%d')
                record = Record(day, record_number)
                continue
            if record is not None and entry['FieldName'] == 'POSID':
                record.project_code = entry['FieldValue']
                continue
            if record is not None and entry['FieldName'] == 'TASKLEVEL':
                record.task = entry['FieldValue']
                continue
            if record is not None and entry['FieldName'] == 'TASKTYPE':
                record.task_type = entry['FieldValue']
                continue
            if record is not None and entry['FieldName'] == 'TIME':
                record.hours = entry['FieldValue']
                continue
            if record is not None and entry['FieldName'] == 'NOTES':
                record.notes = entry['FieldValue']
                continue
            if record is not None and entry['FieldName'] == 'STARTTIME':
                record.start_time = entry['FieldValue']
                continue
            if record is not None and entry['FieldName'] == 'ENDTIME':
                record.end_time = entry['FieldValue']
                continue
            if record is not None and entry['FieldName'] == 'COUNTER':
                record.counter = entry['FieldValue']
                continue
            if record is not None and entry['FieldName'] == 'REASON':
                record.reason = entry['FieldValue']
                continue
            if record is not None and entry['FieldName'] == 'STATUS':
                record.status = entry['FieldValue']
                # MSAVE for draft
                # last field, add record to list
                records.append(record)
                continue
        return records


def work_to_string(day):
    str = "-------------\n"
    str += "DAY: {}\n".format(day['Date'])
    is_working_day = day['WorkingDay']
    str += "WORKING DAY: {}\n".format(is_working_day)
    if is_working_day == 'TRUE':
        str += "TARGET HOURS: {}\n".format(day['TargetHours'])
    str += "-------------"
    return str
