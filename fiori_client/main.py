# coding=utf-8
import argparse
import datetime
import getpass
import os

from fiori_client.default import DEFAULT_PROJECT, DEFAULT_TASK, DEFAULT_USERNAME, DEFAULT_DOMAIN, PRINTER_NAME, \
    DEFAULT_GOAL_HOURS
from fiori_client.fiori import FioriClient

__author__ = 'Valentin Grouès'

# for python 2.7 compatibility
try:
    input = raw_input
except NameError:
    pass


def fill_timesheet():
    fiori_client = login()
    today = datetime.date.today()
    date_one_month_ago = today - datetime.timedelta(days=30)
    year = input('Enter the year of timesheet to fill (keep empty for "{}"): '.format(date_one_month_ago.year))
    if not year:
        year = date_one_month_ago.year
    else:
        year = int(year)
    month = input('Enter the month of timesheet to fill (keep empty for "{}"): '.format(date_one_month_ago.month))
    if not month:
        month = date_one_month_ago.month
    else:
        month = int(month)
    project = input('Enter your project code (keep empty for "{}"): '.format(DEFAULT_PROJECT))
    if not project:
        project = DEFAULT_PROJECT
    task = input('Enter your task code (keep empty for "{}"): '.format(DEFAULT_TASK))
    if not task:
        task = DEFAULT_TASK
    goal_hours = input('How many hours per month on this task? (keep empty for "{}"): '.format(DEFAULT_GOAL_HOURS))
    if goal_hours:
        goal_hours = float(goal_hours)
    else:
        goal_hours = DEFAULT_GOAL_HOURS

    fiori_client.fill_timesheet(year, month, project, task, goal_hours)

    fiori_client.fill_timesheet(year, month, project, task)


def approve_timesheets():
    fiori_client = login()
    people_entries = fiori_client.get_approval_data()
    for entry in people_entries:
        print("Getting details for", entry['EMPNAME'])
        employee_id = entry['PERNR']
        details = fiori_client.get_records_details_to_approve(entry)
        print("{} entries can be validated".format(len(details)))
        print("List of entries to approve for", entry['EMPNAME'], ':')
        for detail in details:
            print("{} hours on {}: {}".format(detail['CATSHOURS'], detail['WORKDATE'], detail['MAIN_FIELD_TEXT']))
        approve = input("Approve? y/n: ")
        if not approve:
            approve = 'y'
        approve = approve.strip() == 'y'
        if approve:
            for detail in details:
                print("{} hours on {}: {}".format(detail['CATSHOURS'], detail['WORKDATE'], detail['MAIN_FIELD_TEXT']))
                ok = fiori_client.approve_entry(employee_id, detail)
                if ok:
                    print('approved')
                else:
                    print('an error occurred')
        else:
            print("skipping")
    print("no more employees")


def submit_timesheet():
    fiori_client = login()
    today = datetime.date.today()
    date_one_month_ago = today - datetime.timedelta(days=30)
    year = input('Enter the year of timesheet to submit (keep empty for "{}"): '.format(date_one_month_ago.year))
    if not year:
        year = date_one_month_ago.year
    else:
        year = int(year)
    month = input('Enter the month of timesheet to submit (keep empty for "{}"): '.format(date_one_month_ago.month))
    if not month:
        month = date_one_month_ago.month
    else:
        month = int(month)
    fiori_client.submit_timesheet(year, month)


def login():
    password = getpass.getpass('Enter your password: ')
    username = input('Enter your username (keep empty for "{}"): '.format(DEFAULT_USERNAME))
    if not username:
        username = DEFAULT_USERNAME
    fiori_client = FioriClient(DEFAULT_DOMAIN)
    fiori_client.login(username, password)
    return fiori_client


def generate_pdf():
    fiori_client = login()
    today = datetime.date.today()
    date_one_month_ago = today - datetime.timedelta(days=30)

    year = input('Enter the year of timesheet to fill (keep empty for "{}"): '.format(date_one_month_ago.year))
    if not year:
        year = date_one_month_ago.year
    else:
        year = int(year)
    month = input('Enter the month of timesheet to fill (keep empty for "{}"): '.format(date_one_month_ago.month))
    if not month:
        month = date_one_month_ago.month
    else:
        month = int(month)
    file_path = "timesheet_{0}{1:02d}.pdf".format(year, month)
    fiori_client.get_pdf(year, month, file_path)
    return file_path


def do_all():
    fiori_client = login()
    today = datetime.date.today()
    date_one_month_ago = today - datetime.timedelta(days=30)
    year = input('Enter the year of timesheet to fill (keep empty for "{}"): '.format(date_one_month_ago.year))
    if not year:
        year = date_one_month_ago.year
    else:
        year = int(year)
    month = input('Enter the month of timesheet to fill (keep empty for "{}"): '.format(date_one_month_ago.month))
    if not month:
        month = date_one_month_ago.month
    else:
        month = int(month)
    project = input('Enter your project code (keep empty for "{}"): '.format(DEFAULT_PROJECT))
    if not project:
        project = DEFAULT_PROJECT
    task = input('Enter your task code (keep empty for "{}"): '.format(DEFAULT_TASK))
    if not task:
        task = DEFAULT_TASK

    fiori_client.fill_timesheet(year, month, project, task)
    submit = input("Submit timesheet? (keep empty for yes)")
    if submit is None or submit == "":
        submit = "yes"
    if submit == "yes":
        fiori_client.submit_timesheet(year, month)
        file_path = "timesheet_{0}{1:02d}.pdf".format(year, month)
        fiori_client.get_pdf(year, month, file_path)
        should_print = input("Print timesheet? (keep empty for yes)")
        if should_print is None or should_print == "":
            should_print = "yes"
        if should_print == "yes":
            print_file(file_path)


def print_file(file_path):
    command = "lp -d {} '{}'".format(PRINTER_NAME, file_path)
    os.system(command)


def download_paystubs():
    fiori_client = login()
    entries = fiori_client.get_paystubs()
    for entry in entries:
        fiori_client.get_paystub_pdf(entry)


def main():
    parser = argparse.ArgumentParser(description='Command line interface of the fiori client',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('action', choices=['timesheet', 'pdf', 'submit', 'all', "print", "approve", "paystubs"],
                        help='specify the action to execute:\n'
                             'timesheet => fill the timesheet,\n'
                             'submit => submit the timesheet,\n'
                             'approve => timesheet approvals,\n'
                             'pdf => generate pdf timesheet\n'
                             'paystubs => download paystubs')
    args = parser.parse_args()
    if args.action == 'timesheet':
        fill_timesheet()
    if args.action == 'all':
        do_all()
    if args.action == 'submit':
        submit_timesheet()
    if args.action == 'pdf':
        generate_pdf()
    if args.action == 'approve':
        approve_timesheets()
    if args.action == 'print':
        file_path = generate_pdf()
        print_file(file_path)
    if args.action == 'paystubs':
        download_paystubs()


if __name__ == '__main__':
    main()
