#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

requirements = [
    'requests_ntlm', 'lxml'
]
APP = ['fiori_client/main.py']
DATA_FILES = []
OPTIONS = {}

setup(
    app=APP,
    options={'py2app': OPTIONS},
    data_files=DATA_FILES,
    setup_requires=['py2app'],
    name='fiori-client',
    version='0.0.1',
    description="Fiori Python Client",
    author="Valentin Grouès",
    author_email='valentin.groues@uni.lu',
    url='https://git-r3lab.uni.lu/valentin.groues/fiori-client',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    package_dir={'fiori_client':
                     'fiori_client'},
    include_package_data=True,
    install_requires=requirements,
    zip_safe=False,
    keywords=['fiori', 'lcsb'],
    classifiers=[
        # 'Development Status :: 2 - Pre-Alpha',
        # 'Intended Audience :: Developers',
        # 'License :: OSI Approved :: ISC License (ISCL)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    entry_points={
        "console_scripts": ['fiori-client = fiori_client.main:main']
    }
)
