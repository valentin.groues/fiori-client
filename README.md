# Fiori Client 

## Description

Python client to the SAP Fiori application. In this version, only filling a timesheet for a single project and a single task is supported. Created entries are not submitted automatically, they'll stay in draft status.

## Requirements

Tested only with Python 3.5 but should work with any Python 3 version, might even work with Python 2.7, who knows...

## Installation

```
python setup.py install
```

## Run

Fill timesheet:

```
fiori-client timesheet
```
Download pdf:

```
fiori-client pdf
```
This file is downloaded in the folder where the command is executed.

## Configuration

Default values for username, project code, etc can be changed by editing the file fiori_client/default.py

## TODO / Ideas

### Timesheets
- print timesheet summary
- delete entries (clean a whole month?)
- ~~trigger pdf generation~~
- submit entries?

### Vacations

- manage vacations requests

### Other
- ...

## Contributing

Merge requests will be very welcome!
